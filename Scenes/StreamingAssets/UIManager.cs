using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
public class UIManager : MonoBehaviour
{
    private string InputText;
    public Text TextInputField, CatText;
    [SerializeField] private TextData textdata;
    [SerializeField] private InputField inputfield;
    [SerializeField] private GameObject Yes, No;
    private bool CanInputField;

    private int NumberMessage;
    private void Start()
    {
        Yes.SetActive(false);
        No.SetActive(false);
        textdata = JsonUtility.FromJson<TextData>(File.ReadAllText(Application.streamingAssetsPath + "/Text.json"));
        CatAsk();
    }

    public void CatchInputText()
    {
        InputText = TextInputField.text;
        CatAnswer();
        inputfield.text = " ";
    }

   public void CatAsk()
    {
        CatText.text = textdata.CatText[NumberMessage];
        NumberMessage++;
    }

    public void CatTakeChoice()
    {
        Yes.SetActive(true);
        No.SetActive(true);
    }

    public void CatAnswer()
    {
        CatText.text = textdata.CatText[NumberMessage] + "  " + InputText;
        NumberMessage++;
    }

}



