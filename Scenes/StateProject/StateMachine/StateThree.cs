using UnityEngine;

public class StateThree : MonoBehaviour
{
    float x, CurrentTime;
    [SerializeField] AnimationCurve ScaleCube;
    private void Update()
    {
        CurrentTime += Time.deltaTime; 
        x= ScaleCube.Evaluate(CurrentTime);
        transform.localScale = new Vector3 (x, x, x);
        if(x > 1) { x = 0; }
    }
}
