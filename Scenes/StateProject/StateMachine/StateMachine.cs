
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField] private int StateSwith;
    Animator StateMachin;
    AnimatorClipInfo[] m_CurrentClipInfo;
    private void Start()
    {
        StateMachin = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        
        StateMachin.SetInteger("Switch", StateSwith);
        m_CurrentClipInfo = this.StateMachin.GetCurrentAnimatorClipInfo(0);
        print(m_CurrentClipInfo[0].clip.name);
        ChangeState(m_CurrentClipInfo[0].clip.name);
    }

    void ChangeState(string NameState)
    {
        switch (NameState)
        {
            case "StateOne":
                FindObjectOfType<StateOne>().enabled = true;
                FindObjectOfType<StateTwo>().enabled = false;
                FindObjectOfType<StateThree>().enabled = false;
                break;
            case "StateTwo":
                FindObjectOfType<StateOne>().enabled = false;
                FindObjectOfType<StateTwo>().enabled = true;
                FindObjectOfType<StateThree>().enabled = false;
                break;
            case "StateThree":
                FindObjectOfType<StateOne>().enabled = false;
                FindObjectOfType<StateTwo>().enabled = false;
                FindObjectOfType<StateThree>().enabled = true;
                break;
        }
  
    }

    
}
