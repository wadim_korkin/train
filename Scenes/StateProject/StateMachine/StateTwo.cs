using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTwo : MonoBehaviour
{
    float RotSpeed;
   
    public void Update()
    {
        RotSpeed += Time.deltaTime;
        transform.rotation = Quaternion.Euler( 0, 0, RotSpeed * 100);
    }
}
